#!/usr/bin/env bash

set -euo pipefail

function usage() {
   local script="$(basename "${BASH_SOURCE[0]}")"
   echo "$script: <config-directory>"
   echo
   echo "Arguments:"
   echo "  config-directory: Absolute path of config directory inside container"
   echo
   echo "Options:"
   echo "  -h, --help: display this message and exit"
}

if [ "$#" -lt 1 ]; then
   usage >&2
   exit 1
fi

if echo $@ | grep --silent --extended-regexp '^(-h|--help)$'; then
   usage
   exit 0
fi

config_directory="$1"

rm --force "$config_directory/deluged.pid"

deluged --config="$config_directory" \
        --loglevel=info \
        --logfile"$config_directory/deluged.log"

deluge-web --config="$config_directory" \
           --loglevel=info \
           --logfile"$config_directory/deluge-web.log" \
           --ssl
