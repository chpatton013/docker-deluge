# Docker Deluge Server

Use this container to operate a Deluge daemon and web client.

## Usage

```
docker build --tag=chpatton013/docker-deluge .
docker run --rm --name=deluge --publish=53160:53160 --publish=53160:53160/udp --publish=8112:8112 --publish=58846:58846 chpatton013/docker-deluge <config directory>
```

You can add volumes to this container from the host machine and from other
containers:

* From host machine: `--volume=<host-machine-path>:<container-mount-path>`
* From other containers: `--volumes-from=<container-name>`

You should use a volume as the config directory to ensure your settings are
persisted when the container is restarted.

Your configuration will specify a series of directories to use when downloading
data. You should use a volume as the location of these directories so that your
data persists as well.
