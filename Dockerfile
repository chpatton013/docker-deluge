FROM debian:jessie
ENV DEBIAN_FRONTEND noninteractive

# Variables ####################################################################
ENV deluge_entrypoint "/usr/local/bin/entrypoint"
ENV torrent_port "53160"
ENV web_port "8112"
ENV daemon_port "58846"

# Packages #####################################################################
RUN apt-get update --quiet --quiet \
 && apt-get install --quiet --quiet --assume-yes \
    deluged \
    deluge-web \
 && rm --recursive --force /var/lib/apt/lists/*

# Deluge #######################################################################
COPY deluge_entrypoint.sh "${deluge_entrypoint}"
RUN chmod +x "${deluge_entrypoint}"

EXPOSE "${torrent_port}"/tcp \
       "${torrent_port}"/udp \
       "${web_port}" \
       "${daemon_port}"

# This should be the $deluge_entrypoint variable, but we can't use the
# shell-interpreted version of ENTRYPOINT for signal-handling reasons.
ENTRYPOINT ["/usr/local/bin/entrypoint"]
